using StaticArrays
using Setfield
#= This should be the exact structure that your UDP and log data look like
	 take note that julia pads to the nearest multiple of 4 bytes =#
struct KS_UDPPacket
	packetVersionNumber::Int32
	frameNumber::UInt32        # The frame number
	A::SVector{16, Float32}    # Transformation matrix (from src to ref)
	quality::Float32           # Tracking quality parameter
	tablePosition::Int32       # Table position at time of cross calibration
	frameTime_sec::UInt32      # Time of frame in sec
	flags::UInt32             
end


#=
typedef struct _KSPMC_RESULT {
  double rotmat[9];
  double trans[3];
  s32 frame;
  s32 acqSegIndex;
} kspmc_result_t;

  kspmc_result_t rtp_packet;
  int shot;
  int slloc;
  int timestamp;
=#
struct KS_PMCPacket
	R::SVector{9, Float64}
	p::SVector{3, Float64}
	frameNumber::Int32
	acqSeqIndex::Int32
	shot::Int32
	slice_location::Int32
	timestamp::Int32
end

function retrieve_A_from(packet::KS_UDPPacket)
	return reshape(packet.A, 4, 4)
end

function retrieve_A_from(packet::KS_PMCPacket)
	R = reshape(packet.R, 3, 3)
	A = zeros(4, 4)
	A[1:3, 1:3] = R
	A[4, 1:3] = packet.p
	return A
end

#= This is for a custom binary file format I use, the first uint16_t is how many characeters are in the header.
The format can then be interpretted by the python struct package (https://docs.python.org/3/library/struct.html). 
In the case of julia we just re-interpret the data using the structure defined above; so we discard the header =#
function loadPayload(Payload::Type, filename)
	psize::UInt32 = sizeof(Payload)
	temp = zeros(UInt8, sizeof(Payload))
	f = open(filename)
	fsize = stat(f).size
	#hdr_len = reinterpret(Int16, read(f, 2))[1]
	dump = read(f, hdr_len)
	npayloads = Integer((fsize - 2 - hdr_len) / psize)
	data = Array{Payload, 1}(undef, npayloads)
	for η in 1:npayloads
			temp[1:psize] = read(f, psize)
			data[η] = reinterpret(Payload, temp)[1]
	end
	close(f)
	return data
end

function loadPayloadPMClogFile(filename)
	psize::UInt32 = sizeof(KS_PMCPacket)
	temp = zeros(UInt8, sizeof(KS_PMCPacket))
	f = open(filename)
	fsize = stat(f).size
	npayloads = Integer(fsize / psize)
	data = Array{KS_PMCPacket, 1}(undef, npayloads)
	for η in 1:npayloads
			temp[1:psize] = read(f, psize)
			x = reinterpret(KS_PMCPacket, temp)[1]
			for i in 1:9
				x = @set x.R[i] = ntoh(x.R[i])
			end
			for j in 1:3
				x = @set x.p[j] = ntoh(x.p[j])
			end
			# The real-time computer is big-endian we need to do some byte swapping here
			# @set is used to modify immutable structures, not sure what it does
			x = @set x.frameNumber = ntoh(x.frameNumber)
			x = @set x.acqSeqIndex = ntoh(x.acqSeqIndex)
			x = @set x.shot = ntoh(x.shot)
			x = @set x.slice_location = ntoh(x.slice_location)
			x = @set x.timestamp = ntoh(x.timestamp)
			data[η] = x
	end
	close(f)
	return data
end

#=function load_ksudppackets(logname::String)
	packets = loadPayload(KS_UDPPacket, logname)
	npkts = length(packets)
	A = Array{Float32, 3}(undef, npkts, 4, 4)
	for i in 1:length(packets)
		A[i, :, :] = retrieve_A_from(packets[i])
	end
	return A
end=#