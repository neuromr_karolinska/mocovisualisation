using Sockets: Int32
using Base: text_colors
using Sockets
using GLMakie; GLMakie.activate!()
using Rotations
using TickTock

using FileIO

include("ks_configuration.jl") 

mutable struct pltlims
    frame_min::Float32
    frame_max::Float32
    xMax::Float32
    xMin::Float32
    yMin::Float32
    yMax::Float32
    zMin::Float32
    zMax::Float32
end


fig = Figure(backgroundcolor = RGBf(0.1, 0.1, 0.1),
    resolution = (1920, 1080), fontsize=32, textcolor=RGBf(0.8, 0.8, 0.8))
axis_canvas_color = RGBf(0.8, 0.8, 0.8)
## Define Observable that trigger all plot updates
udp_packet = Observable(zeros(UInt8, sizeof(KS_UDPPacket)))
on_new = @lift(reinterpret(KS_UDPPacket, $udp_packet)[1])

function toAxisAngle(A)
    R = RotMatrix{3}(A[1:3, 1:3]);
    e = rotation_axis(R) .* (rotation_angle(R) * 180/π)
    return e
end

function updateLines(packet::KS_UDPPacket, lims::pltlims)
    r_vec = toAxisAngle(retrieve_A_from(packet))

    if packet.frameNumber ≤ 2
        ptsX.val = [Point2f(zeros(2)) for i in 0:1:memoryDepth]
        ptsY.val = [Point2f(zeros(2)) for i in 0:1:memoryDepth]
        ptsZ.val = [Point2f(zeros(2)) for i in 0:1:memoryDepth]
    end

    ptsX.val[1] = Point2f((Float32(packet.frameNumber), r_vec[1]))
    ptsX[] = circshift(ptsX.val, -1)

    ptsY.val[1] = Point2f((Float32(packet.frameNumber), r_vec[2]))
    ptsY[] = circshift(ptsY.val, -1)

    ptsZ.val[1] = Point2f((Float32(packet.frameNumber), r_vec[3]))
    ptsZ[] = circshift(ptsZ.val, -1)

    lims.frame_min = ptsX.val[1].data[1] - 2
    lims.frame_max = packet.frameNumber + 2

    if (r_vec[1] > lims.xMax)
        lims.xMax = r_vec[1]
    elseif (r_vec[1] < lims.xMin)
        lims.xMin = r_vec[1]
    end

    if (r_vec[2] > lims.yMax)
        lims.yMax = r_vec[2]
    elseif (r_vec[2] < lims.yMin)
        lims.yMin = r_vec[2]
    end

    if (r_vec[3] > lims.zMax)
        lims.zMax = r_vec[3]
    elseif (r_vec[3] < lims.zMin)
        lims.zMin = r_vec[3]
    end

    limits!(axX, 
        lims.frame_min, 
        lims.frame_max, 
        lims.xMin - 0.2, 
        lims.xMax + 0.2)

    limits!(axY, 
        lims.frame_min, 
        lims.frame_max, 
        lims.yMin - 0.2, 
        lims.yMax + 0.2)
    
    limits!(axZ, 
        lims.frame_min, 
        lims.frame_max, 
        lims.zMin - 0.2, 
        lims.zMax + 0.2)
end

## Line plot of pose
memoryDepth = 128
axX = fig[1, 1] = Axis(fig, title = "X", backgroundcolor=axis_canvas_color)
ptsX = Observable([Point2f(zeros(2)) for i in 0:1:memoryDepth])
scatter!(axX, ptsX, color=:red, markersize=4)

axY = fig[2, 1] = Axis(fig, title = "Y", backgroundcolor=axis_canvas_color)
ptsY = Observable([Point2f(zeros(2)) for i in 0:1:memoryDepth])
scatter!(axY, ptsY, color=:green, markersize=4)

axZ = fig[3, 1] = Axis(fig, title = "Z", backgroundcolor=axis_canvas_color)
ptsZ = Observable([Point2f(zeros(2)) for i in 0:1:memoryDepth])
scatter!(axZ, ptsZ, color=:blue, markersize=4)

pltLimits = pltlims(0, 0, 0, 0, 0, 0, 0, 0)
lines_Observable = @lift(updateLines($on_new, pltLimits))

## The dot tracking experiment.
ax = fig[1:3, 2:3] = Axis(fig, title = "Track the cross!", backgroundcolor=axis_canvas_color)

#=TR = 60e-3
ω = π/6 
κ = 4
α = 1

function updateDot(packet::KS_UDPPacket)
    return Point2f((α * κ * cos(ω * TR * packet.frameNumber), 
                         κ * sin(ω * TR * packet.frameNumber)))
end
=#
path = "example_data/"
exp = "circles/"
file = "ksUPDpacket_log2.bin"

logfile = loadPayloadPMClogFile(path * exp * file)

println("The logfile TR is: ", logfile[2].timestamp - logfile[1].timestamp)

nframes_per_update = logfile[2].frameNumber - logfile[1].frameNumber
println(nframes_per_update)
l = length(logfile)

function updateDot(packet::KS_PMCPacket)
    index = Int(round(packet.frameNumber / nframes_per_update)) + 1
    r = toAxisAngle(retrieve_A_from(logfile[index % l]))
    return Point2f((0, 0))
end

function updateCross(packet::KS_PMCPacket)
    r = toAxisAngle(retrieve_A_from(packet))
    return Point2f((r[3], -r[1]))
end

#=ax.xticks = -α * κ : α * κ : α * κ
ax.yticks = -κ : κ : κ
ax.aspect = AxisAspect(1 / α)=#
#limits!(ax, -12, 12, -6, 6)
limits!(ax, -20, 20, -12.5, 12.5)

function updateCrosses(packet::KS_UDPPacket)
    r_present = toAxisAngle(retrieve_A_from(packet))
    empty!(ax)
    scatter!(ax, Point2f(r_present[3], -r_present[1]), marker='❌', markersize=254, color=:red, rotation=r_present[2] * π / 180.)
    index = Int(round(packet.frameNumber / nframes_per_update)) + 1
    r_target = toAxisAngle(retrieve_A_from(logfile[index]))
    scatter!(ax, Point2f(r_target[3], -r_target[1]), marker='❎', markersize=254, color=:green, rotation=r_target[2] * π / 180.)     
end
@lift(updateCrosses($on_new))

#=scatter!(ax, @lift(updateCross($on_new)), marker='❎', markersize=254, color=:red)
scatter!(ax, @lift(updateDot($on_new)), marker='❌', markersize=254, color=:green)=#


### Plots the 3D model of a head
path = "3d/head.stl"

object = FileIO.load(path)

ax3 = fig[1:3, 4] = Axis3(fig, title = "3D Model", backgroundcolor=RGBf(0.2, 0.2, 0.2), 
    aspect=:data, perspectiveness=0.6,
    xlabel="z", ylabel="x", zlabel="y")
head = mesh!(ax3, object, color=:white, shininess = 128f0)

function moveHead!(head, packet::KS_UDPPacket)
    c = RotX(π / 2) * RotY(π / 2)
    R = c * RotMatrix{3}([packet.A[1:3]; packet.A[5:7]; packet.A[9:11]])
    p = c * Vec3f(packet.A[4:4:12]) ./ 100.0
    rotate!(head, rotation_axis(R), rotation_angle(R))
    translate!(head, p)
end 

head_Observable = @lift(moveHead!(head, $on_new))

#=
# Create a video stream # 
stream = VideoStream(fig, framerate = 2)
function saveFig(packet::KS_UDPPacket)
    if ((packet.frameNumber > 320) && (packet.frameNumber < 548) && (packet.frameNumber % (nframes_per_update * 8) == 0))
        recordframe!(stream)
    end
end

gif_Observable = @lift(saveFig($on_new)) =#

s = UDPSocket()
bind(s, ip"10.0.1.222", 6080)
#bind(s, ip"0.0.0.0", 6080)

on(events(fig).window_open, priority = Int64(20)) do open
    if !open
        close(s)
    end
    return true
end

display(fig)

tick()
while(true)
    try
        bytes = recv(s)
        t1 = peektimer()
        if (length(bytes) == sizeof(KS_UDPPacket)) && (t1 > 0.05)
            udp_packet[] = bytes
            tick()
        end
    catch er
        if isa(er, EOFError)
            println("recv() was interrupted, by window close.")
        else
            throw(er)
            close(s)
        end
        break
    end

end

#=
# Save the stream
save("movie.gif", stream)
=#