# mocoVisualisation

A Julia script that can be used to reproduce motion logs.

## What you need
Julia, with the relevant packages installed [Makie, Rotations, FileIO, StaticArrays, Setfield, TickTock]. 

To install julia on Linux or Mac, type `curl -fsSL https://install.julialang.org | sh` into your command line and follow the prompts to install julia. 
To import the required packages, run julia, and in the julia command prompt type: 
```
import Pkg; Pkg.add("GLMakie")
import Pkg; Pkg.add("Rotations")
import Pkg; Pkg.add("FileIO")
import Pkg; Pkg.add("StaticArrays")
import Pkg; Pkg.add("Setfield")
import Pkg; Pkg.add("TickTock")
```

You will also need a 3D model to display (you can also comment out that section in script.jl if you like)

## Hardware setup
The computer running the script needs to be able to listen in on the prospective motion correction updates. You will also need an fMRI compatible monitor. I have tried to move the implementation specific information into the ks_configuration.jl file. If you'd like to add a new implementation I suggest adding a new file (like $(your_abbreviation)_configuration.jl)
![Hardware setup](media/figure1.png)

## Try it out locally without hardware binding
To run the script locally with simulated motion, bind the the udp socket to localhost (uncomment line #204 in script.jl) and when the julia script is running, run `python python_supporting/serve_udp.py`.

## When it works you should get something like this
![What it should look like](media/figure2.gif)