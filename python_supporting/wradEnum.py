from enum import IntEnum, Enum

class bits2mm(object):
    def __init__(self, WRAD_REV="wrad4"):
        if WRAD_REV == "wrad3":
            self.GX = 0.003393653355560362
            self.GY = 0.00342183325909264
            self.GZ = 0.003232998676120966
        if WRAD_REV == "wrad4":
            self.GX = 0.0040482
            self.GY = 0.0038705
            self.GZ = 0.0039171

class wradModule(IntEnum):
    NEG = 0
    POS = 1
    ONE = 2
    DUMMY = 3
    LAST = 4
    nX = 5
    X = 6
    nY = 7
    Y = 8
    nZ = 9
    Z = 10
    nXZ = 11
    XZ = 12
    SEQSENSE = 13
class wradMode(IntEnum):
    SLEEP = 0
    CONFIG = 1
    RUN_SEND_GOERT = 2
    RUN_SEND_REF = 3
    RUN_SEND_REL_POSE = 4
    RUN_SEND_ABS_POSE = 5
    RUN_SEND_RAWX = 6
    RUN_SEND_RAWY = 7
    RUN_SEND_RAWZ = 8
    TEMP_WARNING = 9
    STATUS = 10

class wradCommand(IntEnum):
    SET_WAIT_us = 0
    SET_NQUART = 1
    SET_SAMPLE_FREQ_kHz = 2
    SET_RF_DETECT_SPACING_us = 3
    SET_RF_DETECT_THRESHOLD_mV = 4
    SET_RF_WIDTH = 5
    SET_NSTART = 6
    SET_NREV = 7
    SET_SELF_TRIGGER = 8
    SET_SEND_STATUS = 9
    SET_SKIP_REV_FLAG = 10
    SET_DOGNLE_ID = 11
    SET_EXIT_BLINKY = 12
    SET_MODE = 255

class wradConfigAddress(IntEnum):
    WAIT = 0
    NQUART = 1
    SAMPLE_FREQ = 2
    RF_DETECT_SPACING = 3
    RF_DETECT_THRESHOLD = 4
    RF_WIDTH = 5
    N_START = 6
    N_REV = 7
    SELF_TRIGGER = 8
    SEND_STATUS = 9
    SKIP_REV_FLAG = 10
    DONGLE_ID = 11
    RETURN_ALL = 12
    N_PKTS = 13
    BAT_V = 14

    def _fmt():
        return "15H"
    def _name():
        return "CONFIG"

class wradStatus(IntEnum):
    SEQSENSE_TABLE_POS = 0
    SEQSENSE_SLEW = 1
    SEQSENSE_MISC = 2
    SEQSENSE_NQUART = 3
    SEQSENSE_NCYC = 4
    SEQSENSE_WAIT = 5
    DETECT_CNT = 6
    DETECT_WIDTH_CNT = 7
    DETECT_PULSE_CNT = 8
    DETECT_FALLING_CNT = 9 
    SEQSENSE_IDX = 10
    SEQSENSE_CNT = 11
    SEQSENSE_NPKTS = 12
    MODULE = 13
    BITFLAGS = 14
    TEMP_MV2 = 15
    TEMP_NRF = 16

    def _fmt():
        return "<HbBBB12H"
    def _name():
        return "STATUS"

class goertSlices(object):
    NZ = slice(0, 3)
    FX = slice(3, 6)
    FY = slice(6, 9)
    FZ = slice(9, 12)
    VECS = slice(3, 12)
    MODULE = 12
    PULSE_CNT = 13
    TRIG_INTERVAL = 14
    PHASE = slice(15, 18)

    def _fmt():
        return "12fHhH3h"
    def _name():
        return "GOERT"

class rawSlices(object):
    RAW = slice(0, -4)
    NZ = slice(-4, -1)
    MODULE = -1
    def __init__(self, gradient, readout_length):
        self.X = slice(0, 3 * readout_length, 3)
        self.Y = slice(1, 3 * readout_length, 3)
        self.Z = slice(2, 3 * readout_length, 3)
        self.name = "RAW%s_%d" % (gradient, readout_length)
        self.fmt = '%ih3hH' % (readout_length * 3)
    def _fmt(self):
        return self.fmt
    def _name(self):
        return self.name

class motionAbsoluteSlices(object):
    QUATERNION = slice(0, 4)
    POS = slice(4, 7)
    PULSE_CNT = 7

    def _fmt():
        return "7fH"
    def _name():
        return "MOTION_ABS"

class motionRelativeSlices(object):
    QUATERNION = slice(0, 4)
    POS = slice(4, 7)
    PULSE_CNT = 7

    def _fmt():
        return "7fH"
    def _name():
        return "MOTION_REL"

class referenceSlices(object):
    QUATERNION = slice(0, 4)
    POS = slice(4, 7)
    TABLE_POS = 7

    def _fmt():
        return "7fH"
    def _name():
        return "REFERENCE"
class udpSlices(object):
    VERSION = 0
    FRAME = 1
    KSMATRIX = slice(2, 18)
    QUALITY = 18
    TABLE_POS = 19
    TIME = 20
    FLAGS = 21