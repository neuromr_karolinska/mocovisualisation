import os
import sys
from struct import Struct, pack
import numpy as np

if sys.platform == "linux":
    LOG_PATH = "/usr/g/research/wrad/logs/"
elif sys.platform == "darwin":
    LOG_PATH = os.path.expanduser("~/rawdata/wrad_logs/")
if not os.path.exists(LOG_PATH):
    os.mkdir(LOG_PATH)

def wradReadLog(filename):
    f = open(filename, 'rb')
    hdr_len = Struct('<H')
    nbytes_in_format = hdr_len.unpack(f.read(hdr_len.size))[0]
    hdr = f.read(nbytes_in_format)
    packet = Struct(hdr)
    nbytes_in_file = os.path.getsize(filename) - \
        (nbytes_in_format + hdr_len.size)
    lst_size = int(nbytes_in_file / packet.size)
    lst = [0] * lst_size
    for i in range(lst_size):
        lst[i] = packet.unpack(f.read(packet.size))

    f.close()
    return np.array(lst)

def wradWriteLog(log_name, struct_format, binary_data):
    f = open(log_name, "wb")
    ## WRITE HEADER ##
    f.write(pack('<h', len(struct_format)))
    f.write(bytearray(struct_format, 'utf8'))
    ## WRITE DATA ##
    for line in binary_data:
        f.write(line)
    f.close()

class wradLog(object):
    file = None
    def __init__(self, name, structure):
        self.flag = False
        self.structure = structure
        self.name = name
        self.data = []

    def start(self):
        self.data.clear()

    def stop(self, folder, index):
        if not os.path.exists(LOG_PATH + folder):
            os.mkdir(LOG_PATH + folder)
        logname = "%s%s/%s_log%d.bin" % (LOG_PATH, folder, self.name, index)
        # WRITE OUT BINARY DATA
        wradWriteLog(logname, self.structure.format, self.data)
        self.flag = False
    
    def write(self, line):
        self.data.append(line)

    def step(self, folder, index):
        self.stop(folder, index)
        self.start()
