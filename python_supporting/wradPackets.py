import numpy as np
from struct import Struct, pack
from math import ceil
import socket

from wradLog import wradLog

class wradUDPpacket(object):
    packet_structure = Struct('2i17fi2I')
    packetVersionNumber=np.int32(1)
    frameNumber=np.int32(0)
    quality=np.float64(1.0)
    tablePosition=np.int32(0)
    frameTime_sec=np.uint32(0)
    flags=np.uint32(0)

    def __init__(self, IPaddress='10.0.1.100', Port=6000, Broadcast=False):
        self.ip = IPaddress
        self.port = Port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        if Broadcast:
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            self.port = 37020
            self.ip = '<broadcast>'

        self.log = wradLog("ksUPDpacket", self.packet_structure)
        
    def send(self, A):
        bytes_to_send = self.packet_structure.pack(self.packetVersionNumber,
                                                   self.frameNumber,
                                                   *A.flatten(),
                                                   self.quality,
                                                   self.tablePosition,
                                                   self.frameTime_sec,
                                                   self.flags)
        self.s.sendto(bytes_to_send, (self.ip, self.port))
        self.s.sendto(bytes_to_send, ("10.0.1.222", 6080))
        self.log.write(bytes_to_send)


class wradPayload(object):
    npackets = int(0)
    nrf_packet_size = int(30)

    def __init__(self, slices):
        self.structure = Struct(slices._fmt())
        self.npackets = ceil(float(self.structure.size) / float(self.nrf_packet_size)) - 1
        self.log = wradLog(slices._name(), self.structure)

    def unpack(self, byte_array):
        data = byte_array[:self.structure.size]
        self.log.write(data)
        return self.structure.unpack(data)
