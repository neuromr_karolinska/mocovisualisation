from struct import pack
from wradPackets import wradUDPpacket
from wradLog import wradReadLog as read
from wradEnum import udpSlices as us
from scipy.spatial.transform import Rotation

import numpy as np

import time

packet = wradUDPpacket(IPaddress="0.0.0.0", Port=6080)
log = "example_data/circles/ksUPDpacket_log2.bin"
lines = read(log)
A = np.eye(4)
R = Rotation.from_euler("xyz", (np.pi / 100, 0, 0))
for line in lines:
    packet.frameNumber = int(line[us.FRAME])
    packet.send(line[us.KSMATRIX].reshape(4, 4))
    time.sleep(0.05)